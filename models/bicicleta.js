//Constructor
var Bicicleta = function(id, color, modelo, ubicacion) {
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;
}

//Prototipo
Bicicleta.prototype.toString = function() {
  return 'id: ' + this.id + ' | color: ' + this.color;
}

//Vector temporal sin BD
Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
  Bicicleta.allBicis.push(aBici);
}

//Método para buscar
Bicicleta.findById = function(aBiciId) {
  var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
  if (aBici)
    return aBici;
  else
    throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
}

//Método para eliminar
Bicicleta.removeById = function(aBiciId) {
  for(var i=0; i < Bicicleta.allBicis.length; i++){
    if (Bicicleta.allBicis[i].id == aBiciId){
      Bicicleta.allBicis.splice(i, 1);
      break;
    }
  }
}

//Agregamos objetos
var a  = new Bicicleta(1, 'rojo', 'urbana', [6.21, -75.57]);
var b  = new Bicicleta(2, 'blanco', 'urbana', [6.23, -75.56]);
var c  = new Bicicleta(3, 'azul', 'urbana', [6.267548, -75.565787]);
var d  = new Bicicleta(4, 'negro', 'urbana', [6.277520, -75.570318]);
var e  = new Bicicleta(5, 'verde', 'urbana', [6.223629, -75.574527]);
Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);
Bicicleta.add(d);
Bicicleta.add(e);

//se exporta el modulo
module.exports = Bicicleta;
